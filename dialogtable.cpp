#include "dialogtable.h"
#include <QFormLayout>

DialogTable::DialogTable(QStringList *s){
	QFormLayout *formLayout = new QFormLayout;
	editD=new QLineEdit(s->at(0));
	editN=new QLineEdit(s->at(1));
    formLayout->addRow(QObject::tr("Обозначение элемента в схеме"), editD);
    formLayout->addRow(QObject::tr("Расшифровка элемента в отчете"), editN);
	QHBoxLayout *hLayout = new QHBoxLayout;
	pYes = new QPushButton(s->at(2));
	pCancel = new QPushButton(s->at(3));
	hLayout->addWidget(pYes);
	hLayout->addWidget(pCancel);
	QVBoxLayout* vLayout = new QVBoxLayout;
	vLayout->addLayout(formLayout);
	vLayout->addLayout(hLayout);
	this->setLayout(vLayout);
	this->setWindowTitle(s->at(4));
	this->setWindowModality(Qt::WindowModal);
}
