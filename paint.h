#ifndef PAINT_H
#define PAINT_H
#include "qprinter.h"
#include "qpainter.h"
#include "qlist.h"
#include "cells.h"
#include "qrect.h"
#include "qprogressdialog.h"

typedef struct{
	float table1; // граница первой ячейки
	float table2; // граница второй ячейки
	float table3; // граница третьей ячейки
	float format1; // граница 1- ячейки форматки
	float format2; // граница 2- ячейки форматки
	float format3; // граница 3- ячейки форматки
	float format4; // граница 4- ячейки форматки
	float format5; // граница 5- ячейки форматки
	float format6; // граница 6- ячейки форматки
	float format7; // граница 7- ячейки форматки
	float format8; // граница 8- ячейки форматки
	float format9; // граница 9- ячейки форматки
	float format10; // граница 9 последней ячейки
	float gost_first; // высота рамки гост для первой страницы
	float gost; // высота рамки гост для следующих страниц
	float line; // толщина линии рамок
}PAINT_DATA;

class Paint
{
public:
	Paint();
	~Paint();
	void savePDF(QPrinter*,Cells*);
	void gost_widget(QPainter*);
    void saveHTML(Cells*,QString);
    void saveRTF(Cells*,QString);
    void saveTXT(Cells*,QString);
    void save_RTF(Cells*,QString);

private:
	void gost_first(QPainter*); // рамка по госту для первой страницы
	void gost(QPainter*); // рамка по госту для первой страницы
	void setText(QPainter*,float,float,int,QString); // вывод текста в ячейки рамки по госту
	void title(QPainter*);
	bool table(QPainter*,Cells*); // таблица
	int insText(QPainter*,float,float,QString); // вывод многострочного техта
	void vert(QPainter*,int h); // вертикальные линии в таблице
	PAINT_DATA data; // размеры рамок в мм
	float k; // масштаб (писель на миллиметр)
	int y; // координата Y
	float bias_y; // смещение для текста
	float bias_x; // смещение для текста
	int paper; // номер аткивного паинтере в массиве
	int c; // номер поледней выведеной строки из cell
	int h; // высота ячейки
	int pr; // значения для прогрессбара
};

#endif // PAINT_H
