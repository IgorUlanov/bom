#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QTextStream>
#include <QProcess>
#include <QTextEdit>
//#include <QDebug>
#include <QWidget>
#include <QPrinter>
#include <QSettings>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QSpacerItem>
#include <QFormLayout>
#include <QLabel>
#include <QMessageBox>
#include <QMovie>
#include <QFuture>
#include <QtConcurrentRun>
#include <QFontDatabase>

#include "paint.h"
#include "cells.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QSettings* set = new QSettings("settings.ini",QSettings::IniFormat);
    set->setIniCodec("UTF-8");
    ui->developerEdit->setText(set->value("developer").toString());
    ui->checkerEdit->setText(set->value("checker").toString());
    ui->t_checkerEdit->setText(set->value("t_checker").toString());
    ui->n_checkerEdit->setText(set->value("n_checker").toString());
    ui->approvedEdit->setText(set->value("approved").toString());
    ui->productEdit->setText(set->value("product").toString());
    ui->suffixEdit->setText(set->value("suffix").toString());
    ui->description1Edit->setText(set->value("description1").toString());
    ui->description2Edit->setText(set->value("description2").toString());
    ui->description3Edit->setText(set->value("description3").toString());
    ui->companyEdit->setText(set->value("company").toString());
    ui->product_sizeSpin->setValue(set->value("product_size").toDouble());
    ui->description_sizeSpin->setValue(set->value("description_size").toDouble());
    ui->company_sizeSpin->setValue(set->value("company_size").toDouble());
    if (set->value("pdf")=="yes") ui->checkPDF->setChecked(true); else ui->checkPDF->setChecked(false);
    if (set->value("html")=="yes") ui->checkHTML->setChecked(true); else ui->checkHTML->setChecked(false);
    if (set->value("txt")=="yes") ui->checkTXT->setChecked(true); else ui->checkTXT->setChecked(false);
    if (set->value("rtf")=="yes") ui->checkRTF->setChecked(true); else ui->checkRTF->setChecked(false);
    ui->gost_widget->setFixedHeight(55.0*ui->gost_widget->width()/180.0);
    ui->table_one->setStyleSheet(
                    "QTableView::item:selected:active {"
                    "background: rgb(255, 255,255);"
                    "border: 1px solid transparent;"
                    "selection-color:rgb(0, 0,0);"
                    "}"
                    "QTableView::item:selected:!active {"
                    "background: rgb(255, 255,255);"
                    "border: 1px solid transparent;"
                    "selection-color:rgb(0, 0,0);"
                    "}"
                    );
    ui->table_two->setStyleSheet(
                    "QTableView::item:selected:active {"
                    "background: rgb(255, 255,255);"
                    "border: 1px solid transparent;"
                    "selection-color:rgb(0, 0,0);"
                    "}"
                    "QTableView::item:selected:!active {"
                    "background: rgb(255, 255,255);"
                    "border: 1px solid transparent;"
                    "selection-color:rgb(0, 0,0);"
                    "}"
                    );
    tableRead();
    path="";
    QFontDatabase::addApplicationFont(":/gostA.ttf");
    QFontDatabase::addApplicationFont(":/gostB.ttf");
}

MainWindow::~MainWindow(){
    delete ui;
}

void MainWindow::on_openButton_clicked(){
    QString n = QFileDialog::getOpenFileName(this, tr("Open File"),path);
    if (n!=""){
        name=QFileInfo(n).fileName();
        path=QFileInfo(n).filePath();
        dir=QFileInfo(n).absolutePath();
        Cells* cell=new Cells(path);
        if (cell->error.size()!=0){
            QString s;
            for (int i=0;i<cell->error.size();i++) s+=QString::number(i)+". "+cell->error.at(i)+"\n";
            QMessageBox* msgBox=new QMessageBox(QMessageBox::Critical,tr("Ошибка в файле"),tr("Файл ")+path+tr(" не считан.\n")+s,QMessageBox::Ok);
            msgBox->exec();
            path="";
        }
        else{
            if (cell->warning.size()!=0){
                QString s;
                for (int i=0;i<cell->warning.size();i++) s+=QString::number(i)+". "+cell->warning.at(i)+"\n";
                QMessageBox* msgBox=new QMessageBox(QMessageBox::Warning,tr("Замечание при разборе."),s,QMessageBox::Ok);
                msgBox->exec();
            }
            QSettings* set = new QSettings("settings.ini",QSettings::IniFormat);
            set->setIniCodec("UTF-8");
            set->setValue("product",name);
            ui->productEdit->setText(name);
			ui->gost_widget->update();
        }
    }
}

void MainWindow::on_product_sizeSpin_valueChanged(double arg1){
    QSettings* set = new QSettings("settings.ini",QSettings::IniFormat);
    set->setIniCodec("UTF-8");
    set->setValue("product_size", arg1);
    set->sync();
    ui->gost_widget->update();
}

void MainWindow::on_description_sizeSpin_valueChanged(double arg1){
    QSettings* set = new QSettings("settings.ini",QSettings::IniFormat);
    set->setIniCodec("UTF-8");
    set->setValue("description_size", arg1);
    set->sync();
    ui->gost_widget->update();
}

void MainWindow::on_company_sizeSpin_valueChanged(double arg1){
    QSettings* set = new QSettings("settings.ini",QSettings::IniFormat);
    set->setIniCodec("UTF-8");
    set->setValue("company_size", arg1);
    set->sync();
    ui->gost_widget->update();
}

void MainWindow::on_productEdit_textChanged(const QString &arg1){
    QSettings* set = new QSettings("settings.ini",QSettings::IniFormat);
    set->setIniCodec("UTF-8");
    set->setValue("product", arg1);
    set->sync();
    ui->gost_widget->update();
}

void MainWindow::on_suffixEdit_textChanged(const QString &arg1){
    QSettings* set = new QSettings("settings.ini",QSettings::IniFormat);
    set->setIniCodec("UTF-8");
    set->setValue("suffix", arg1);
    set->sync();
    ui->gost_widget->update();
}


void MainWindow::on_companyEdit_textChanged(const QString &arg1){
    QSettings* set = new QSettings("settings.ini",QSettings::IniFormat);
    set->setIniCodec("UTF-8");
    set->setValue("company", arg1);
    set->sync();
    ui->gost_widget->update();
}

void MainWindow::paintEvent(QPaintEvent *) {
    if (ui->tabWidget->currentIndex()==0){
        int x[5];
        x[0]=ui->productEdit->fontMetrics().width(ui->productEdit->text()+" "+ui->suffixEdit->text());
        x[1]=ui->companyEdit->fontMetrics().width(ui->companyEdit->text());
        x[2]=ui->description1Edit->fontMetrics().width(ui->description1Edit->text());
        x[3]=ui->description2Edit->fontMetrics().width(ui->description2Edit->text());
        x[4]=ui->description3Edit->fontMetrics().width(ui->description3Edit->text());
        int n=x[0];
        for (int i=1;i<5;i++) if (n<x[i]) n=x[i];
        n+=this->fontMetrics().height();
        ui->productEdit->setMinimumWidth(n);
        x[0]=ui->developerEdit->fontMetrics().width(ui->developerEdit->text());
        x[1]=ui->checkerEdit->fontMetrics().width(ui->checkerEdit->text());
        x[2]=ui->t_checkerEdit->fontMetrics().width(ui->t_checkerEdit->text());
        x[3]=ui->n_checkerEdit->fontMetrics().width(ui->n_checkerEdit->text());
        x[4]=ui->approvedEdit->fontMetrics().width(ui->approvedEdit->text());
        n=x[0];
        for (int i=1;i<5;i++) if (n<x[i]) n=x[i];
        n+=this->fontMetrics().height();
        ui->developerEdit->setMinimumWidth(n);
        ui->gost_widget->setFixedHeight(55.0*ui->gost_widget->width()/180.0);
    }
    else{
        ui->table_one->setColumnWidth(1,ui->table_one->width()-ui->table_one->columnWidth(0)-ui->table_one->verticalHeader()->width());
        ui->table_two->setColumnWidth(1,ui->table_two->width()-ui->table_two->columnWidth(0)-ui->table_two->verticalHeader()->width());
    }

}

void MainWindow::on_developerEdit_textChanged(const QString &arg1){
    QSettings* set = new QSettings("settings.ini",QSettings::IniFormat);
    set->setIniCodec("UTF-8");
    set->setValue("developer", arg1);
    set->sync();
    ui->gost_widget->update();
}

void MainWindow::on_checkerEdit_textChanged(const QString &arg1){
    QSettings* set = new QSettings("settings.ini",QSettings::IniFormat);
    set->setIniCodec("UTF-8");
    set->setValue("checker", arg1);
    set->sync();
    ui->gost_widget->update();
}

void MainWindow::on_t_checkerEdit_textChanged(const QString &arg1){
    QSettings* set = new QSettings("settings.ini",QSettings::IniFormat);
    set->setIniCodec("UTF-8");
    set->setValue("t_checker", arg1);
    set->sync();
    ui->gost_widget->update();
}

void MainWindow::on_n_checkerEdit_textChanged(const QString &arg1){
    QSettings* set = new QSettings("settings.ini",QSettings::IniFormat);
    set->setIniCodec("UTF-8");
    set->setValue("n_checker", arg1);
    set->sync();
    ui->gost_widget->update();
}

void MainWindow::on_approvedEdit_textChanged(const QString &arg1){
    QSettings* set = new QSettings("settings.ini",QSettings::IniFormat);
    set->setIniCodec("UTF-8");
    set->setValue("approved", arg1);
    set->sync();
    ui->gost_widget->update();
}

void MainWindow::on_checkPDF_stateChanged(int arg1){
    QSettings* set = new QSettings("settings.ini",QSettings::IniFormat);
    set->setIniCodec("UTF-8");
    QString s="no";
    if (arg1==Qt::Checked) s="yes";
    set->setValue("pdf", s);
    set->sync();
}

void MainWindow::on_checkHTML_stateChanged(int arg1){
    QSettings* set = new QSettings("settings.ini",QSettings::IniFormat);
    set->setIniCodec("UTF-8");
    QString s="no";
    if (arg1==Qt::Checked) s="yes";
    set->setValue("html", s);
    set->sync();
}

void MainWindow::on_checkTXT_stateChanged(int arg1){
    QSettings* set = new QSettings("settings.ini",QSettings::IniFormat);
    set->setIniCodec("UTF-8");
    QString s="no";
    if (arg1==Qt::Checked) s="yes";
    set->setValue("txt", s);
    set->sync();
}

void MainWindow::on_checkRTF_stateChanged(int arg1){
    QSettings* set = new QSettings("settings.ini",QSettings::IniFormat);
    set->setIniCodec("UTF-8");
    QString s="no";
    if (arg1==Qt::Checked) s="yes";
    set->setValue("rtf", s);
    set->sync();
}

void MainWindow::editD_changed(QString s){
    table_edit[0]=s;
}

void MainWindow::editN_changed(QString s){
    table_edit[1]=s;
}

void MainWindow::pCancel_clicked(){
    table_edit[0]="";
    table_edit[1]="";
    dialogTable->close();
}

void MainWindow::pDelete_clicked(){
    if (table_edit[0]!=""){
        if (table_edit[0].size()==1){
            // одна буква
            QSettings* set = new QSettings("settings.ini",QSettings::IniFormat);
            set->setIniCodec("UTF-8");
            set->beginGroup("one");
            set->remove(table_edit[0]);
            set->endGroup();
            set->sync();
            tableRead();
        }
        if (table_edit[0].size()==2){
            // две буквы
            QSettings* set = new QSettings("settings.ini",QSettings::IniFormat);
            set->setIniCodec("UTF-8");
            set->beginGroup("two");
            set->remove(table_edit[0]);
            set->endGroup();
            set->sync();
            tableRead();
        }
    }
    table_edit[0]="";
    table_edit[1]="";
    dialogTable->close();
}

void MainWindow::pSave_clicked(){
    if (table_edit[0]!=""){
        if (table_edit[0].size()==1){
            // одна буква
            QSettings* set = new QSettings("settings.ini",QSettings::IniFormat);
            set->setIniCodec("UTF-8");
            set->beginGroup("one");
            set->setValue(table_edit[0],table_edit[1]);
            set->endGroup();
            set->sync();
            tableRead();
        }
        if (table_edit[0].size()==2){
            // две буквы
            QSettings* set = new QSettings("settings.ini",QSettings::IniFormat);
            set->setIniCodec("UTF-8");
            set->beginGroup("two");
            set->setValue(table_edit[0],table_edit[1]);
            set->endGroup();
            set->sync();
            tableRead();
        }
    }
    table_edit[0]="";
    table_edit[1]="";
    dialogTable->close();
}

void MainWindow::tableRead(){
    QSettings* set = new QSettings("settings.ini",QSettings::IniFormat);
    set->setIniCodec("UTF-8");
    ui->table_one->setColumnCount(2);
    ui->table_one->setRowCount(1);
    QStringList headers;
    headers<<tr("обозначение")<<tr("расшифровка");
    ui->table_one->setHorizontalHeaderLabels(headers);
    int row=1;
    set->beginGroup("one");
    QStringList group=set->allKeys();
    for (int i=0;i<group.size();++i){
        QTableWidgetItem *newItem1 = new QTableWidgetItem(group.at(i));
        ui->table_one->setRowCount(row);
        ui->table_one->setItem(row-1,0,newItem1);
        QTableWidgetItem *newItem2 = new QTableWidgetItem(set->value(group.at(i)).toString());
        ui->table_one->setItem(row-1,1,newItem2);
        row++;
    }
    set->endGroup();
    ui->table_two->setColumnCount(2);
    ui->table_two->setRowCount(1);
    ui->table_two->setHorizontalHeaderLabels(headers);
    set->beginGroup("two");
    group=set->allKeys();
    row=1;
    for (int i=0;i<group.size();++i){
        QTableWidgetItem *newItem1 = new QTableWidgetItem(group.at(i));
        ui->table_two->setRowCount(row);
        ui->table_two->setItem(row-1,0,newItem1);
        QTableWidgetItem *newItem2 = new QTableWidgetItem(set->value(group.at(i)).toString());
        ui->table_two->setItem(row-1,1,newItem2);
        row++;
    }
    set->endGroup();
}

QString MainWindow::getPath(){
    return path;
}

QString MainWindow::getDir(){
    return dir;
}

QString MainWindow::getName(){
    return name;
}

static void startSave(MainWindow* pthis){
    Paint* paint=new Paint();
    QSettings* set = new QSettings("settings.ini",QSettings::IniFormat);
    set->setIniCodec("UTF-8");
    QString path=pthis->getPath();
    Cells* cell=new Cells(path);
    QString out=pthis->getDir()+"/out";
    QDir dir(out);
    if (!dir.exists()) {
        dir.mkpath(".");
    }
    out+="/"+pthis->getName();
    if (set->value("html")=="yes") paint->saveHTML(cell,out);
    if (set->value("txt")=="yes") paint->saveTXT(cell,out);
    if (set->value("rtf")=="yes") paint->saveRTF(cell,out);
    if (set->value("pdf")=="yes"){
        QPrinter printer(QPrinter::HighResolution);
        printer.setPageSize(QPrinter::A4);
        printer.setPageMargins(12.5,15,12.5,15,QPrinter::Millimeter);
        printer.setColorMode(QPrinter::GrayScale);
        printer.setOutputFormat(QPrinter::PdfFormat);
        printer.setOutputFileName(out+QObject::tr(" Перечень элементов.pdf"));
        paint->savePDF(&printer,cell);
    }
}

void MainWindow::on_pushSave_clicked(){
    if (path!=""){
        frm = new QWidget;
        QMovie* movie = new QMovie(":/wait.gif");
        QLabel* gif = new QLabel(this);
        gif->setAlignment(Qt::AlignCenter);
        gif->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        gif->setMovie(movie);
        movie->start();
        QFont f("Arial",14, QFont::Normal, true);
        QLabel* Label = new QLabel(tr("Подождите немного...\nФормируются документы..."));
        Label->setAlignment(Qt::AlignCenter);
        Label->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        Label->setFont(f);
        QHBoxLayout* mainLayout = new QHBoxLayout;
        mainLayout->addWidget(gif);
        mainLayout->addWidget(Label);
        frm->setLayout(mainLayout);
        frm->setWindowTitle(tr("формирование документов"));
        frm->setLayout(mainLayout);
        frm->setWindowModality(Qt::WindowModal);
        frm->setGeometry(x()+(width()-500)/2,y()+(height()-200)/2,500,200);
        frm->show();
        connect(&watcher, SIGNAL(finished()), this, SLOT(handleFinished()));
        QFuture<void> future = QtConcurrent::run(&startSave,this);
        watcher.setFuture(future);
    }
}

void MainWindow::handleFinished(){
     frm->close();
}

void MainWindow::on_table_one_customContextMenuRequested(const QPoint &pos){
    QMenu menu(this);
    QAction *ins = menu.addAction(tr("вставить строку")); // there can be more than one
    QAction *del = menu.addAction(tr("удалить строку")); // there can be more than one
    QAction *a = menu.exec(ui->table_one->viewport()->mapToGlobal(pos));
    if (a == ins){
        QStringList s;
        s<<""<<""<<tr("Сохранить и закрыть")<<tr("Закрыть без сохранения")<<tr("Добавление строки в таблицу");
        dialogTable=new DialogTable(&s);
        dialogTable->move(x()+width()/3,y()+height()/3);
        connect(dialogTable->editD, SIGNAL(textChanged(QString)), this, SLOT(editD_changed(QString)));
        connect(dialogTable->editN, SIGNAL(textChanged(QString)), this, SLOT(editN_changed(QString)));
        connect(dialogTable->pCancel, SIGNAL(clicked()), this, SLOT(pCancel_clicked()));
        connect(dialogTable->pYes, SIGNAL(clicked()), this, SLOT(pSave_clicked()));
        dialogTable->show();
    }
    if (a==del){
        QStringList s;
        table_edit[0]=ui->table_one->item(ui->table_one->currentRow(),0)->text();
        table_edit[1]=ui->table_one->item(ui->table_one->currentRow(),1)->text();
        s<<table_edit[0]<<table_edit[1];
        s<<tr("Удалить и закрыть")<<tr("Закрыть без удаления")<<tr("Удаление строки из таблицы");
        dialogTable=new DialogTable(&s);
        dialogTable->move(x()+width()/3,y()+height()/3);
        dialogTable->editD->setReadOnly(true);
        dialogTable->editN->setReadOnly(true);
        connect(dialogTable->pCancel, SIGNAL(clicked()), this, SLOT(pCancel_clicked()));
        connect(dialogTable->pYes, SIGNAL(clicked()), this, SLOT(pDelete_clicked()));
        dialogTable->show();
    }
}

void MainWindow::on_table_two_customContextMenuRequested(const QPoint &pos){
    QMenu menu(this);
    QAction *ins = menu.addAction(tr("вставить строку")); // there can be more than one
    QAction *del = menu.addAction(tr("удалить строку")); // there can be more than one
    QAction *a = menu.exec(ui->table_two->viewport()->mapToGlobal(pos));
    if (a == ins){
        QStringList s;
        s<<""<<""<<tr("Сохранить и закрыть")<<tr("Закрыть без сохранения")<<tr("Добавление строки в таблицу");
        dialogTable=new DialogTable(&s);
        dialogTable->move(x()+width()/3,y()+height()/3);
        connect(dialogTable->editD, SIGNAL(textChanged(QString)), this, SLOT(editD_changed(QString)));
        connect(dialogTable->editN, SIGNAL(textChanged(QString)), this, SLOT(editN_changed(QString)));
        connect(dialogTable->pCancel, SIGNAL(clicked()), this, SLOT(pCancel_clicked()));
        connect(dialogTable->pYes, SIGNAL(clicked()), this, SLOT(pSave_clicked()));
        dialogTable->show();
    }
    if (a==del){
        QStringList s;
        table_edit[0]=ui->table_two->item(ui->table_two->currentRow(),0)->text();
        table_edit[1]=ui->table_two->item(ui->table_two->currentRow(),1)->text();
        s<<table_edit[0]<<table_edit[1];
        s<<tr("Удалить и закрыть")<<tr("Закрыть без удаления")<<tr("Удаление строки из таблицы");
        dialogTable=new DialogTable(&s);
        dialogTable->move(x()+width()/3,y()+height()/3);
        dialogTable->editD->setReadOnly(true);
        dialogTable->editN->setReadOnly(true);
        connect(dialogTable->pCancel, SIGNAL(clicked()), this, SLOT(pCancel_clicked()));
        connect(dialogTable->pYes, SIGNAL(clicked()), this, SLOT(pDelete_clicked()));
        dialogTable->show();
    }
}

