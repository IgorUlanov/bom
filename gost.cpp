#include "gost.h"
#include <QPainter>
#include "paint.h"

Gost::Gost(QWidget *parent) : QWidget(parent)
{

}

void Gost::paintEvent(QPaintEvent *) {
	QPainter p(this); // Создаём новый объект рисовальщика
	Paint* paint=new Paint();
	paint->gost_widget(&p);
	p.end();
}
