#include "cells.h"
#include <QFile>
#include <QTextStream>
#include <QSettings>
#include <QProgressDialog>
#include <QDebug>


Cells::Cells(QString name){
	QString part="xxxxxxx";
	QFile f_source(name);
	f_source.open(QIODevice::ReadOnly | QIODevice::Text);
	QTextStream source(&f_source);
    source.setCodec("windows-1251");
	QString str=source.readLine();
    data.clear();
	// Плата
	while (!source.atEnd()){
		str=source.readLine();
        if (str!=""){
			QStringList list2 = str.split('"',QString::SkipEmptyParts);
			QStringList list1;
			for (int i = 0; i < list2.size(); ++i){
				QString s=list2.at(i);
				if (s!=","){
					if (i==0){
						QString p=findPart(s);
						if (p!=part){
							part=p;
							if (p==""){
								int n=s.indexOf(",");
								if (n<0) n=s.size();
								warning.append(QObject::tr("Нет расшифровки для \"")+s.left(n)+"\"");
							}
                            data.append(QStringList()<<""<<""<<""<<"");
                            data.append(QStringList()<<""<<part<<""<<"");
						}
					}
					list1+=list2.at(i);
				}
			}
            if (list1.size()==3) list1+=QString(" ");
            if(list1.size()==4) data.append(list1);
			else error.append(QObject::tr("Не разобрать формат строки ")+QString::number(data.size()));
        }
	}
	f_source.close();
}

Cells::~Cells()
{

}

QString Cells::load(int f,int s){
    return data.at(f).at(s);
}

int Cells::size(void){
	return data.size();
}

QString Cells::findPart(QString string){
	QSettings* set = new QSettings("settings.ini",QSettings::IniFormat);
	set->setIniCodec("UTF-8");
	set->beginGroup("one");
	QStringList group=set->allKeys();
	QString s;
	for (int i=0;i<group.size();++i){
		if (group.at(i).toLatin1()==string.left(1).toLatin1()) s=set->value(group.at(i)).toString();
	}
	set->endGroup();
	if (s==""){
		set->beginGroup("two");
		group=set->allKeys();
		for (int i=0;i<group.size();++i){
			if (group.at(i).toLatin1()==string.left(2).toLatin1()) s=set->value(group.at(i)).toString();
		}
		set->endGroup();
	}
	return s;
}
