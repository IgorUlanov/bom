#include "paint.h"
#include <QSettings>
#include <QImage>
#include <QDebug>
#include <QFile>
#include <QTextCursor>
#include <QTextDocument>
#include <QTextEdit>
#include <QTextCursor>
#include <QTextTable>
#include <QTextDocumentWriter>
#include <QTextStream>

Paint::Paint(){
	data.format1=7;
	data.format2=data.format1+10.0;
	data.format3=data.format2+23.0;
	data.format4=data.format3+15.0;
	data.format5=data.format4+10.0;
	data.format6=data.format5+70.0;
	data.format7=data.format6+15.0;
	data.format8=data.format7+15.0;
	data.format9=data.format8+8.0;
	data.format10=data.format8+20.0;
	data.table1=35;
	data.table2=105;
	data.table3=120;
	data.gost_first=40;
	data.gost=15;
	data.line=0.2;
	c=0;
	paper=0;
}

Paint::~Paint(){

}

void Paint::savePDF(QPrinter* pr, Cells* cell){
	k=pr->paperRect().right()/210.0; // A4
	c=0;
	QList<QImage> list;
	list.append(QImage(pr->pageRect().width(),pr->pageRect().height(),QImage::Format_MonoLSB));
	QPainter* pic=new QPainter(&list.last());
	pic->setPen(QPen(Qt::white, data.line*k, Qt::SolidLine, Qt::RoundCap));
	pic->setBrush(QBrush(Qt::black));
	pic->fillRect(list.last().rect(),QBrush(Qt::black));
	y=0;
	while (!table(pic,cell)){
		pic->end();
		list.append(QImage(pr->pageRect().width(),pr->pageRect().height(),QImage::Format_MonoLSB));
		pic->begin(&list.last());
		pic->setPen(QPen(Qt::white, data.line*k, Qt::SolidLine, Qt::RoundCap));
		pic->setBrush(QBrush(Qt::black));
		pic->fillRect(list.last().rect(),QBrush(Qt::black));
		y=0;
	}
	pic->end();
/*	int f_id = QFontDatabase::addApplicationFont(":/gostB.ttf");
	QFont f = QFont(QFontDatabase::applicationFontFamilies(f_id).first());
	f.setPointSizeF(2.4*k);// ("GOST type B",2.4*k, QFont::Normal, true);
	f.setItalic(true);*/
//	QFont f("GOST type B",2.4*k, QFont::Normal, true);
	QFont f("GOST type A",2.4*k, QFont::Normal, true);
	for (int i=0;i<list.size();i++){
		pic->begin(&list[i]);
		pic->setFont(f);
		pic->setPen(QPen(Qt::white, data.line*k, Qt::SolidLine, Qt::RoundCap));
		pic->setBrush(QBrush(Qt::black));
		bias_y=((5*k)-pic->fontMetrics().height())/2;
		if (i==0){
			y=pic->viewport().height()-data.gost_first*k;
			setText(pic,data.format8*k,data.format10*k,25,QString::number(list.size()));
		}
		else{
			y=pic->viewport().height()-data.gost*k;
			setText(pic,data.format9*k,data.format10*k,12,QString::number(i+1));
		}
		pic->end();
	}
	QPainter* paint=new QPainter(pr);
	for (int i=0;i<list.size()-1;i++){
		paint->drawImage(0,0,list.at(i),Qt::MonoOnly);
		pr->newPage();
	}
	paint->drawImage(0,0,list.last(),Qt::MonoOnly);
	paint->end();
}

bool Paint::table(QPainter* p, Cells* cell){
	// таблица
	bool finish=true;
/*	int f_id = QFontDatabase::addApplicationFont(":/gostB.ttf");
	QFont f = QFont(QFontDatabase::applicationFontFamilies(f_id).first());
	f.setPointSizeF(3.0*k);
	f.setItalic(true);*/
//	QFont f("GOST type B",3.0*k, QFont::Normal, true);
	QFont f("GOST type A",3.0*k, QFont::Normal, true);
	p->setFont(f);
	h=p->fontMetrics().height()*1.5;
	bias_y=p->fontMetrics().height()/4;
	bias_x=p->fontMetrics().width("8");
	title(p);
	for (int i=c;i<cell->size();i++){
		// вывод строк
        int n=insText(p,0,data.table1*k,cell->load(i,0));
		if (cell->load(i,0)==""){
			f.setBold(true);
			p->setFont(f);
		}
		int n1=insText(p,data.table1*k,data.table2*k,cell->load(i,1));
        if (cell->load(i,1)==""){
			f.setBold(false);
			p->setFont(f);
		}
        setText(p,data.table2*k,data.table3*k,h/k,cell->load(i,2));
        int n2=insText(p,data.table3*k,data.format10*k,cell->load(i,3));
        if (cell->load(i,3)==""){
            f.setBold(false);
            p->setFont(f);
        }
        if (n<n1) n=n1;
        if (n<n2) n=n2;
		int g=p->viewport().height()-data.gost_first*k;
		if (paper!=0) g=p->viewport().height()-data.gost*k;
		if (y+h+(n*0.75*h)+bias_y>g){
			// не помещается на страницу.
			p->fillRect(0,y,p->viewport().width(),p->viewport().height()-y,QBrush(Qt::black));
			p->drawLine(0,y,data.format10*k,y);
			c=i;
			paper++;
			finish=false;
			break;
		}
//		setText(p,data.table2*k,data.table3*k,h/k,cell->load(i,2));
		vert(p,h+(n*0.75*h));
		y+=h+(n*0.75*h);
		p->drawLine((data.line*k)/2,y,data.format10*k-(data.line*k)/2,y);
	}
	if (finish) gost(p); else gost_first(p);
	return finish;
}

int Paint::insText(QPainter* p,float x1,float x2,QString str){
	// вывод многострочного текста
	float x=x1+bias_x;
	int n=0;
	int y1=y;
	QStringList list2 = str.split(' ', QString::SkipEmptyParts);
	for (int i = 0; i < list2.size(); ++i){
		QString s=list2.at(i);
		float w=p->fontMetrics().width(s+" ");
		if (x+w>x2){
			n++;
			y1+=h*0.75;
			x=x1+bias_x;
		}
		p->drawText(x,y1+h-bias_y,s+" ");
		x+=w;
	}
	return n;
}

void Paint::title(QPainter* p){
	y=0;
	p->drawRect((data.line*k)/2,0,data.format10*k-(data.line*k)/2,h*2);
	setText(p,0.0,data.table1*k,1.1*h/k,QObject::tr("Поз."));
	setText(p,0.0,data.table1*k,1.8*h/k,QObject::tr("обознач."));
	setText(p,data.table1*k,data.table2*k,1.4*h/k,QObject::tr("Наименование"));
	setText(p,data.table2*k,data.table3*k,1.4*h/k,QObject::tr("Кол."));
	setText(p,data.table3*k,data.format10*k,1.4*h/k,QObject::tr("Примечание"));
	vert(p,h*2);
	y=h*2;
}

void Paint::vert(QPainter* p,int h_){
	// вертикальные линии
	p->drawLine((data.line*k)/2,y,(data.line*k)/2,y+h_);
	p->drawLine(data.table1*k,y,data.table1*k,y+h_);
	p->drawLine(data.table2*k,y,data.table2*k,y+h_);
	p->drawLine(data.table3*k,y,data.table3*k,y+h_);
	p->drawLine(data.format10*k-(data.line*k)/2,y,data.format10*k-(data.line*k)/2,y+h_);
}

void Paint::gost_widget(QPainter* p){
	k=p->viewport().width()/190.0; // A4
	p->setPen(QPen(Qt::black, data.line*k, Qt::SolidLine, Qt::RoundCap));
	p->setBrush(QBrush(Qt::white));
	y=0;
	gost_first(p);
}

void Paint::gost_first(QPainter* p){
	// рамка по госту для первой страницы
	QSettings* set = new QSettings("settings.ini",QSettings::IniFormat);
	set->setIniCodec("UTF-8");
	y=p->viewport().height()-data.gost_first*k;
	p->drawRect((data.line*k)/2,y,data.format10*k-(data.line*k)/2,data.gost_first*k);
	// горизонтальные линии
	for (int i=1;i<8;i++) p->drawLine (0,y+5*k*i, data.format5*k,y+5*k*i );
	p->drawLine (data.format5*k,y+15*k,data.format10*k-(data.line*k)/2,y+15*k);
	p->drawLine (data.format6*k,y+20*k,data.format10*k-(data.line*k)/2,y+20*k);
	p->drawLine (data.format6*k,y+25*k,data.format10*k-(data.line*k)/2,y+25*k);
	// вертикальные линии
	p->drawLine (data.format1*k,y,data.format1*k,y+15*k);
	p->drawLine (data.format2*k,y,data.format2*k,p->viewport().height());
	p->drawLine (data.format3*k,y,data.format3*k,p->viewport().height());
	p->drawLine (data.format4*k,y,data.format4*k,p->viewport().height());
	p->drawLine (data.format5*k,y,data.format5*k,p->viewport().height());
	p->drawLine (data.format6*k,y+15*k,data.format6*k,p->viewport().height());
	p->drawLine (data.format7*k,y+15*k,data.format7*k,y+25*k);
	p->drawLine (data.format8*k,y+15*k,data.format8*k,y+25*k);
	// заполнение
/*	int f_id = QFontDatabase::addApplicationFont(":/gostA.ttf");
	QFont f = QFont(QFontDatabase::applicationFontFamilies(f_id).first());
	f.setPointSizeF(2.6*k);
	f.setItalic(true);*/
	QFont f("GOST type A",2.6*k, QFont::Normal, true);
	p->setFont(f);
	QString s=QObject::tr("Изм.");
	bias_y=((5*k)-p->fontMetrics().height())/2;
	bias_x=((data.format1*k)-p->fontMetrics().width(s))/2;
	p->drawText(0+bias_x,y+15*k-bias_y,s);
	s=QObject::tr("Лист");
	bias_x=(((data.format2-data.format1)*k)-p->fontMetrics().width(s))/2;
	p->drawText(data.format1*k+bias_x,y+15*k-bias_y,s);
	s=QObject::tr("№ докум.");
	bias_x=(((data.format3-data.format2)*k)-p->fontMetrics().width(s))/2;
	p->drawText(data.format2*k+bias_x,y+15*k-bias_y,s);
	s=QObject::tr("Подпись");
	bias_x=(((data.format4-data.format3)*k)-p->fontMetrics().width(s))/2;
	p->drawText(data.format3*k+bias_x,y+15*k-bias_y,s);
	s=QObject::tr("Дата");
	bias_x=(((data.format5-data.format4)*k)-p->fontMetrics().width(s))/2;
	p->drawText(data.format4*k+bias_x,y+15*k-bias_y,s);
	s=QObject::tr("Разработал");
	bias_x=((data.format2*k)-p->fontMetrics().width(s))/2;
	p->drawText(bias_x,y+20*k-bias_y,s);
	p->drawText(data.format2*k+bias_x,y+20*k-bias_y,set->value("developer").toString());
	s=QObject::tr("Проверил");
	p->drawText(bias_x,y+25*k-bias_y,s);
	p->drawText(data.format2*k+bias_x,y+25*k-bias_y,set->value("checker").toString());
	s=QObject::tr("Т контр");
	p->drawText(bias_x,y+30*k-bias_y,s);
	p->drawText(data.format2*k+bias_x,y+30*k-bias_y,set->value("t_checker").toString());
	s=QObject::tr("Н контр");
	p->drawText(bias_x,y+35*k-bias_y,s);
	p->drawText(data.format2*k+bias_x,y+35*k-bias_y,set->value("n_checker").toString());
	s=QObject::tr("Утвердил");
	p->drawText(bias_x,y+40*k-bias_y,s);
	p->drawText(data.format2*k+bias_x,y+40*k-bias_y,set->value("approved").toString());
	setText(p,data.format6*k,data.format7*k,20,QObject::tr("Литер"));
	setText(p,data.format7*k,data.format8*k,20,QObject::tr("Лист"));
	setText(p,data.format7*k,data.format8*k,25,"1");
	setText(p,data.format8*k,data.format10*k,20,QObject::tr("Листов"));
	// компания
/*	f_id = QFontDatabase::addApplicationFont(":/gostB.ttf");
	QFont ft = QFont(QFontDatabase::applicationFontFamilies(f_id).first());
	ft.setPointSizeF(set->value("company_size").toDouble()*k);
	ft.setItalic(true);*/
//	QFont ft("GOST type B",set->value("company_size").toDouble()*k, QFont::Normal, true);
	QFont ft("GOST type A",set->value("company_size").toDouble()*k, QFont::Normal, true);
	p->setFont(ft);
	bias_y=((15*k)-p->fontMetrics().height())/2;
	setText(p,data.format6*k,data.format10*k,40,set->value("company").toString());
	// название детали
	ft.setPointSizeF(set->value("product_size").toDouble()*k);
	p->setFont(ft);
	bias_y=((15*k)-p->fontMetrics().height())/2;
    setText(p,data.format5*k,data.format10*k,13,set->value("product").toString()+" "+set->value("suffix").toString());
	// документ
	ft.setPointSizeF(set->value("description_size").toDouble()*k);
	p->setFont(ft);
	bias_y=((8*k)-p->fontMetrics().height())/2;
	setText(p,data.format5*k,data.format6*k,24,set->value("description1").toString());
	setText(p,data.format5*k,data.format6*k,30,set->value("description2").toString());
	setText(p,data.format5*k,data.format6*k,36,set->value("description3").toString());
}

void Paint::gost(QPainter* p){
	// рамка по госту для следующих страниц
	QSettings* set = new QSettings("settings.ini",QSettings::IniFormat);
	set->setIniCodec("UTF-8");
	y=p->viewport().height()-data.gost*k;
	p->drawRect((data.line*k)/2,y,data.format10*k-(data.line*k)/2,data.gost*k);
	// горизонтальные линии
	p->drawLine (0,y+5*k, data.format5*k,y+5*k);
	p->drawLine (0,y+10*k, data.format5*k,y+10*k);
	p->drawLine (data.format9*k,y+5*k,data.format10*k-(data.line*k)/2,y+5*k);
	// вертикальные линии
	p->drawLine (data.format1*k,y,data.format1*k,p->viewport().height());
	p->drawLine (data.format2*k,y,data.format2*k,p->viewport().height());
	p->drawLine (data.format3*k,y,data.format3*k,p->viewport().height());
	p->drawLine (data.format4*k,y,data.format4*k,p->viewport().height());
	p->drawLine (data.format5*k,y,data.format5*k,p->viewport().height());
	p->drawLine (data.format9*k,y,data.format9*k,p->viewport().height());
	// заполнение
/*	int f_id = QFontDatabase::addApplicationFont(":/gostA.ttf");
	QFont f = QFont(QFontDatabase::applicationFontFamilies(f_id).first());
	f.setPointSizeF(2.6*k);
	f.setItalic(true);*/
	QFont f("GOST type A",2.6*k, QFont::Normal, true);
	p->setFont(f);
	QString s=QObject::tr("Изм.");
	bias_y=((5*k)-p->fontMetrics().height())/2;
	bias_x=((data.format1*k)-p->fontMetrics().width(s))/2;
	p->drawText(0+bias_x,y+15*k-bias_y,s);
	s=QObject::tr("Лист");
	bias_x=(((data.format2-data.format1)*k)-p->fontMetrics().width(s))/2;
	p->drawText(data.format1*k+bias_x,y+15*k-bias_y,s);
	s=QObject::tr("№ докум.");
	bias_x=(((data.format3-data.format2)*k)-p->fontMetrics().width(s))/2;
	p->drawText(data.format2*k+bias_x,y+15*k-bias_y,s);
	s=QObject::tr("Подпись");
	bias_x=(((data.format4-data.format3)*k)-p->fontMetrics().width(s))/2;
	p->drawText(data.format3*k+bias_x,y+15*k-bias_y,s);
	s=QObject::tr("Дата");
	bias_x=(((data.format5-data.format4)*k)-p->fontMetrics().width(s))/2;
	p->drawText(data.format4*k+bias_x,y+15*k-bias_y,s);
	setText(p,data.format9*k,data.format10*k,5,QObject::tr("Лист"));
	// название детали
/*	f_id = QFontDatabase::addApplicationFont(":/gostB.ttf");
	QFont ft = QFont(QFontDatabase::applicationFontFamilies(f_id).first());
	ft.setPointSizeF(set->value("product_size").toDouble()*k);
	ft.setItalic(true);*/
//	QFont ft("GOST type B",set->value("product_size").toDouble()*k, QFont::Normal, true);
	QFont ft("GOST type A",set->value("product_size").toDouble()*k, QFont::Normal, true);
	p->setFont(ft);
	bias_y=((15*k)-p->fontMetrics().height())/2;
    setText(p,data.format5*k,data.format9*k,13,set->value("product").toString()+" "+set->value("suffix").toString());
}

void Paint::setText(QPainter* p,float n1,float n2, int n, QString s){
	float bias_x=(n2-n1-p->fontMetrics().width(s))/2;
	p->drawText(n1+bias_x,y+n*k-bias_y,s);
}

void Paint::saveHTML(Cells* cell,QString name){
	QString web="<html><head><meta charset=\"utf-8\"><style type=\"text/css\"> table,th,td{border:1px solid black;border-collapse: collapse;font-family: Verdana, Arial, sans-serif; font-style: italic;} th,td{padding: 5px;}</style></head>";
    web+="<body><div align=\"center\" style=\"width: 100%; padding: 0px 0px;\"><table style=\"width:100%\"><tr><th width=\"45%\">Наименование</th><th width=\"10%\">Кол.</th><th width=\"45%\">Примечание</th></tr>";
	QString decor;
	for (int i=0;i<cell->size();i++){
		// вывод строк
		if (cell->load(i,0)==""){
			decor=" align=\"center\" style=\"text-decoration: underline;\"";
		}
		else decor="";
        web+=QString("<tr><td%1>%2</td><td>%3</td><td>%4</td></tr>")
                .arg(decor).arg(cell->load(i,1)).arg(cell->load(i,2)).arg(cell->load(i,3));
	}
	web+="</table></div></body></html>";
    QFile f_html(name+QObject::tr(" Перечень элементов.html"));
	f_html.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Truncate);
	QTextStream html(&f_html);
	html.setCodec("UTF-8");
	html<<web;
	f_html.close();
}

void Paint::saveTXT(Cells* cell,QString name){
	QFile f_txt(name+QObject::tr(" Перечень элементов.txt"));
	f_txt.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Truncate);
	QTextStream txt(&f_txt);
	for (int i=0;i<cell->size();i++){
		// вывод строк
        txt<<cell->load(i,1)+"\t\t"+cell->load(i,2)+"\t\t"+cell->load(i,3)+"\n";
	}
	f_txt.close();
}

void Paint::saveRTF(Cells* cell,QString name){
	QString rtf="{\\rtf1\\ansi\\ansicpg1251 \\i \\sl400";
	for (int i=0;i<cell->size();i++){
		// вывод строк
		QString d1="";
		QString d2="";
		if (cell->load(i,0)==""){
			d1=" \\b";
			d2="\\b0 ";
		}
        rtf+=QString("\\trowd \\clvertalb \\clftsWidth2\\clwWidth22\\cellx1 \\clftsWidth2\\clwWidth5\\cellx2 \\clftsWidth2\\clwWidth22\\cellx3 \\intbl %1").arg(d1);
        QString s=cell->load(i,1);
		for (int n=0;n<s.length();n++){
			rtf+=QString("\\u%1\\'3f").arg(s.at(n).unicode());
		}
		rtf+=QString("%1 \\cell \\initbl ").arg(d2);
		s=cell->load(i,2);
		for (int n=0;n<s.length();n++){
			rtf+=QString("\\u%1\\'3f").arg(s.at(n).unicode());
		}
        rtf+=QString("%1 \\cell \\initbl ").arg(d2);
        s=cell->load(i,3);
        for (int n=0;n<s.length();n++){
            rtf+=QString("\\u%1\\'3f").arg(s.at(n).unicode());
        }
        rtf+=" \\cell \\row";
    }
	rtf+=" }";
	QFile f_rtf(name+QObject::tr(" Перечень элементов.rtf"));
	f_rtf.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Truncate);
	QTextStream t_rtf(&f_rtf);
	t_rtf.setCodec("windows-1251");
	t_rtf<<rtf;
	f_rtf.close();
}
