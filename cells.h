#ifndef CELLS_H
#define CELLS_H
#include "qstring.h"
#include "qlist.h"

class Cells
{
public:
	Cells(QString);
	~Cells();
	QString load(int,int);
	int size(void);
	QList<QString> error;
	QList<QString> warning;
private:
	QList<QStringList> data;
	QString findPart(QString);
};

#endif // CELLS_H
