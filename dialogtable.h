#ifndef DIALOGTABLE_H
#define DIALOGTABLE_H

#include <QWidget>
#include "qstringlist.h"
#include "qlineedit.h"
#include "qpushbutton.h"

class DialogTable: public QWidget
{
public:
	DialogTable(QStringList*);
	QLineEdit* editD;
	QLineEdit* editN;
	QPushButton *pYes;
	QPushButton *pCancel;
};

#endif // DIALOGTABLE_H
