#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "dialogtable.h"
#include "qfuturewatcher.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();
	QWidget* frm;
	QString getPath();
    QString getDir();
    QString getName();

protected:
	void paintEvent(QPaintEvent *);

private slots:
	void on_openButton_clicked();
	void on_product_sizeSpin_valueChanged(double arg1);
	void on_description_sizeSpin_valueChanged(double arg1);
	void on_company_sizeSpin_valueChanged(double arg1);
	void on_productEdit_textChanged(const QString &arg1);
	void on_companyEdit_textChanged(const QString &arg1);
	void on_developerEdit_textChanged(const QString &arg1);
	void on_checkerEdit_textChanged(const QString &arg1);
	void on_t_checkerEdit_textChanged(const QString &arg1);
	void on_n_checkerEdit_textChanged(const QString &arg1);
	void on_approvedEdit_textChanged(const QString &arg1);
	void on_checkPDF_stateChanged(int arg1);
	void on_checkHTML_stateChanged(int arg1);
	void on_checkTXT_stateChanged(int arg1);
	void on_checkRTF_stateChanged(int arg1);
	void on_pushSave_clicked();
	void pSave_clicked();
	void editD_changed(QString);
	void editN_changed(QString);
	void pCancel_clicked();
	void pDelete_clicked();
	void on_table_one_customContextMenuRequested(const QPoint &pos);

	void on_table_two_customContextMenuRequested(const QPoint &pos);
	void handleFinished();

    void on_suffixEdit_textChanged(const QString &arg1);

private:
	Ui::MainWindow *ui;
	QString part;
	QString name;
	QString path;
    QString dir;
	QString table_edit[2];
	void tableRead();
	DialogTable* dialogTable;
	QFutureWatcher<void> watcher;
};

#endif // MAINWINDOW_H
